extends Control

class DataSource:
	func get_items(_filter: String = '') -> Array:
		return []

class ArrayDataSource extends DataSource:
	var data := []

	func get_items(filter: String = '') -> Array:
		if filter == '':
			return data

		var filtered := []
		for item in data:
			if item.to_lower().find(filter) != -1:
				filtered.append(item)
		return filtered

export (String) var label := "Label" setget set_label
export (String) var text := "" setget set_text, get_text
export (Vector2) var max_popup_size := Vector2.ZERO
export (bool) var disabled := false setget set_disabled

onready var label_node: Label = $'%Label'
onready var popup_menu: PopupMenu = $'%PopupMenu'
onready var line_edit: LineEdit = $'%LineEdit'
onready var button: Button = $'%Button'

var data_source: DataSource = ArrayDataSource.new()

signal text_changed (new_text)
signal text_entered (new_text)
signal text_selected (new_text)

func _ready() -> void:
	popup_menu.set_as_toplevel(true)
	line_edit.grab_focus()

func set_label(p_label: String) -> void:
	if label != p_label:
		label = p_label
		if label_node == null:
			yield(self, "ready")
		label_node.text = label

func set_text(p_text: String) -> void:
	if line_edit == null:
		yield(self, "ready")
	line_edit.text = p_text

func get_text() -> String:
	return line_edit.text

func set_disabled(p_disabled: bool) -> void:
	if disabled != p_disabled:
		disabled = p_disabled

		if line_edit == null:
			yield(self, "ready")
		line_edit.editable = !disabled
		button.disabled = disabled

func refresh_popup_menu() -> void:
	_refresh_popup_menu(line_edit.text)

func _refresh_popup_menu(filter: String = '') -> void:
	filter = filter.to_lower().strip_edges()
	popup_menu.clear()
	for item in data_source.get_items(filter):
		popup_menu.add_item(item)

	popup_menu.rect_position = line_edit.rect_global_position + Vector2(0, line_edit.rect_size.y)
	popup_menu.rect_size.x = line_edit.rect_size.x
	popup_menu.rect_size.y = 0

	# @todo Limit to the max_popup_size

func _on_Button_toggled(p_button_pressed: bool) -> void:
	if p_button_pressed:
		_refresh_popup_menu()
		popup_menu.visible = true
	else:
		popup_menu.visible = false

func _on_PopupMenu_index_pressed(index: int) -> void:
	text = popup_menu.get_item_text(index)
	line_edit.text = text
	line_edit.caret_position = text.length()
	popup_menu.focus_mode = FOCUS_NONE
	button.pressed = false
	line_edit.grab_focus()
	emit_signal("text_selected", text)

func _on_LineEdit_text_changed(new_text: String) -> void:
	new_text = new_text.strip_edges()
	if new_text == "":
		popup_menu.visible = false
		button.pressed = false
	else:
		_refresh_popup_menu(new_text)
		if popup_menu.get_item_count() != 0:
			popup_menu.visible = true
		else:
			popup_menu.visible = false
			button.pressed = false

	emit_signal("text_changed", new_text)

func _on_LineEdit_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed('ui_down'):
		if popup_menu.visible:
			line_edit.accept_event()
			popup_menu.focus_mode = FOCUS_ALL
			popup_menu.grab_focus()
			popup_menu.set_current_index(0)

func _on_LineEdit_text_entered(new_text: String) -> void:
	popup_menu.visible = false
	button.pressed = false
	emit_signal("text_entered", new_text)

