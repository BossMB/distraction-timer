extends Control

onready var username_field: LineEdit = $'%UsernameField'
onready var password_field: LineEdit = $'%PasswordField'
onready var create_account_button: Button = $'%CreateAccountButton'
onready var login_button: Button = $'%LoginButton'

const TIMER_SCENE_PATH = 'res://src/main/timer/Timer.tscn'

func _ready() -> void:
	if Globals.saved_username != "" and Globals.saved_password != "":
		username_field.text = Globals.saved_username
		password_field.text = Globals.saved_password
		_on_LoginButon_pressed()

func _activate_ui(active: bool = true) -> void:
	username_field.editable = active
	password_field.editable = active
	create_account_button.disabled = not active
	login_button.disabled = not active

func _deactivate_ui() -> void:
	_activate_ui(false)

func _on_CreateAccountButton_pressed() -> void:
	_deactivate_ui()

	var task : AuthTask = yield(Supabase.auth.sign_up(username_field.text, password_field.text), 'completed')
	if task.error:
		_activate_ui()
		Globals.show_error('Unable to sign up: ' + str(task.error.description))
		return

	_successful_login()

func _on_LoginButon_pressed() -> void:
	_deactivate_ui()

	var task : AuthTask = yield(Supabase.auth.sign_in(username_field.text, password_field.text), 'completed')
	if task.error:
		_activate_ui()
		Globals.show_error('Unable to sign in: ' + str(task.error.description))
		return

	_successful_login()

func _successful_login() -> void:
	Globals.saved_username = username_field.text
	Globals.saved_password = password_field.text
	Globals.save_credentials()

	get_tree().change_scene(TIMER_SCENE_PATH)
