extends Node

signal timer_touched ()

func _input(_event: InputEvent) -> void:
	if self.visible:
		emit_signal("timer_touched")
		get_tree().set_input_as_handled()
