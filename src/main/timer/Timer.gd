extends Control

const AutocompleteDropdown = preload("res://src/ui/autocomplete_dropdown/AutocompleteDropdown.gd")

onready var START_TIMER_TEXT = $'%TimerButton'.text
onready var START_INTERRUPTION_TEXT = $'%InterruptionButton'.text
const STOP_TIMER_TEXT = 'Done'
const STOP_INTERRUPTION_TEXT = 'Back to work...'

enum TimerState {
	LOADING,
	INVALID,
	STOPPED,
	STOPPING,
	STARTED,
	STARTING,
	INTERRUPTED,
	INTERRUPTING,
}

onready var timer_button: Button = $'%TimerButton'
onready var interruption_button: Button = $'%InterruptionButton'
onready var activity_field: AutocompleteDropdown = $'%ActivityField'

onready var distraction_free_timer: Timer = $'%DistractionFreeTimer'
onready var big_timer_label: Label = $'%DistractionFreeMode/BigTimerLabel'
onready var animation_player: AnimationPlayer = $AnimationPlayer

var activity_data_source := AutocompleteDropdown.ArrayDataSource.new()

# Current timer state.
var state: int = TimerState.LOADING
var timer_id: String
var timer_start_time := 0
var interruption_id: String
var interruption_start_time := 0

func _ready() -> void:
	activity_field.data_source = activity_data_source

	_change_state(TimerState.LOADING)
	_load_remote_state()
	_load_activity_list()

func _load_activity_list() -> void:
	var query: SupabaseQuery = SupabaseQuery.new().from("activities").select();
	var dbtask: DatabaseTask = yield(Supabase.database.query(query), "completed")
	if dbtask.error:
		Globals.show_error("Unable to get activity list: " + str(dbtask.error))
		return

	var data := []
	for activity_data in dbtask.data:
		data.append(activity_data['name'])
	activity_data_source.data = data

	activity_field.refresh_popup_menu()

func _load_remote_state() -> void:
	var dbtask: DatabaseTask = yield(Supabase.database.rpc("get_timer_state"), "completed")
	if dbtask.error:
		Globals.show_error("Unable to load remote timer state: " + str(dbtask.error))
		return

	var data: Dictionary = dbtask.data

	if not data['timer_id']:
		_change_state(TimerState.INVALID)
		return

	timer_id = data['timer_id']
	var timer_start_time_dict := parse_date(data['timer_start_time'])
	timer_start_time = OS.get_unix_time_from_datetime(timer_start_time_dict)

	if data['activity_name']:
		activity_field.text = data['activity_name']

	if not data['interruption_id']:
		_change_state(TimerState.STARTED)
		return

	interruption_id = data['interruption_id']
	var interruption_start_time_dict := parse_date(data['interruption_start_time'])
	interruption_start_time = OS.get_unix_time_from_datetime(interruption_start_time_dict)

	_change_state(TimerState.INTERRUPTED)

func _change_state(new_state: int) -> void:
	state = new_state

	distraction_free_timer.stop()

	match state:
		TimerState.LOADING:
			timer_button.disabled = true
			interruption_button.disabled = true

		TimerState.INVALID:
			timer_button.disabled = true
			interruption_button.disabled = true

		TimerState.STOPPED:
			timer_button.text = START_TIMER_TEXT
			timer_button.disabled = false
			interruption_button.text = START_INTERRUPTION_TEXT
			interruption_button.disabled = true

		TimerState.STARTING:
			timer_button.disabled = true

		TimerState.STARTED:
			OS.hide_virtual_keyboard()
			timer_button.text = STOP_TIMER_TEXT
			timer_button.disabled = false
			interruption_button.text = START_INTERRUPTION_TEXT
			interruption_button.disabled = false
			distraction_free_timer.start()

		TimerState.STOPPING:
			timer_button.disabled = true

		TimerState.INTERRUPTING:
			timer_button.disabled = true
			interruption_button.disabled = true

		TimerState.INTERRUPTED:
			OS.hide_virtual_keyboard()
			timer_button.text = STOP_TIMER_TEXT
			timer_button.disabled = false
			interruption_button.text = STOP_INTERRUPTION_TEXT
			interruption_button.disabled = false

	# We disable the activity field if we aren't in the INVALID or STOPPPED states
	activity_field.disabled = not state in [TimerState.INVALID, TimerState.STOPPED]

func _on_ActivityField_text_changed(new_text: String) -> void:
	if new_text == "":
		_change_state(TimerState.INVALID)
	elif state == TimerState.INVALID:
		_change_state(TimerState.STOPPED)

func _on_ActivityField_text_entered(new_text: String) -> void:
	if new_text != "" and state in [TimerState.INVALID, TimerState.STOPPED]:
		start_timer()

func _on_TimerButton_pressed() -> void:
	if state == TimerState.STOPPED:
		start_timer()
	elif state in [TimerState.STARTED, TimerState.INTERRUPTED]:
		stop_timer()

func start_timer() -> void:
	if state != TimerState.STOPPED:
		return

	var activity_name = $'%ActivityField'.text.strip_edges()
	if activity_name == '':
		Globals.show_error("Must enter activity name!")

	var query: SupabaseQuery
	var dbtask: DatabaseTask

	_change_state(TimerState.STARTING)

	query = SupabaseQuery.new().from("activities").select().eq("name", activity_name)
	dbtask = yield(Supabase.database.query(query), "completed")
	if dbtask.error:
		_change_state(TimerState.STOPPED)
		Globals.show_error(str(dbtask.error))
		return

	if dbtask.data.size() == 0:
		query = SupabaseQuery.new().from("activities").insert([{
			name = activity_name,
			owner = Supabase.auth.client.id,
		}])
		dbtask = yield(Supabase.database.query(query), "completed")
		if dbtask.error:
			_change_state(TimerState.STOPPED)
			Globals.show_error(str(dbtask.error))
			return
		activity_data_source.data.append(activity_name)

	var activity_id = dbtask.data[0]['id']

	query = SupabaseQuery.new().from("timers").insert([{
		activity = activity_id,
		owner = Supabase.auth.client.id,
	}])
	dbtask = yield(Supabase.database.query(query), "completed")
	if dbtask.error:
		_change_state(TimerState.STOPPED)
		Globals.show_error(str(dbtask.error))
		return

	var timer_data: Dictionary = dbtask.data[0]
	timer_id = timer_data['id']

	var start_time_dict := parse_date(timer_data['started_at'])
	timer_start_time = OS.get_unix_time_from_datetime(start_time_dict)
	_change_state(TimerState.STARTED)

func stop_timer() -> void:
	if not state in [TimerState.STARTED, TimerState.INTERRUPTED]:
		return

	var original_state = state
	_change_state(TimerState.STOPPING)

	var query = SupabaseQuery.new().from("timers").update({
		stopped_at = 'now()',
	}).eq('id', timer_id)
	var dbtask: DatabaseTask = yield(Supabase.database.query(query), "completed")
	if dbtask.error:
		_change_state(original_state)
		Globals.show_error(str(dbtask.error))
		return

	timer_start_time = 0
	interruption_start_time = 0
	_change_state(TimerState.STOPPED)

func _on_InterruptionButton_pressed() -> void:
	if state == TimerState.STARTED:
		start_interruption()
	elif state == TimerState.INTERRUPTED:
		stop_interruption()

func start_interruption() -> void:
	if state != TimerState.STARTED:
		return

	_change_state(TimerState.INTERRUPTING)

	var query := SupabaseQuery.new().from("interruptions").insert([{
		timer = timer_id,
		owner = Supabase.auth.client.id,
	}])
	var dbtask = yield(Supabase.database.query(query), "completed")
	if dbtask.error:
		_change_state(TimerState.STARTED)
		Globals.show_error(str(dbtask.error))
		return

	var interruption: Dictionary = dbtask.data[0]
	interruption_id = interruption['id']

	var start_time_dict := parse_date(interruption['started_at'])
	interruption_start_time = OS.get_unix_time_from_datetime(start_time_dict)

	_change_state(TimerState.INTERRUPTED)

func stop_interruption() -> void:
	if state != TimerState.INTERRUPTED:
		return

	_change_state(TimerState.INTERRUPTING)

	var query = SupabaseQuery.new().from("interruptions").update({
		stopped_at = 'now()',
	}).eq('id', interruption_id)
	var dbtask: DatabaseTask = yield(Supabase.database.query(query), "completed")
	if dbtask.error:
		_change_state(TimerState.INTERRUPTED)
		Globals.show_error(str(dbtask.error))
		return

	interruption_start_time = 0
	_change_state(TimerState.STARTED)

func parse_date(iso_date: String) -> Dictionary:
	var date := iso_date.split("T")[0].split("-")
	var time := iso_date.split("T")[1].trim_suffix("Z").split(":")

	# @drs Throw away the milliseconds.
	time[2] = time[2].split('.')[0]

	return {
		year = date[0],
		month = date[1],
		day = date[2],
		hour = time[0],
		minute = time[1],
		second = time[2],
	}

func seconds_to_timestamp(total_seconds: int) -> String:
	var time_str: String

	var minutes = total_seconds / 60
	var seconds = total_seconds % 60

	var hours = minutes / 60
	if hours > 0:
		minutes = minutes % 60
		time_str = "%02d:%02d:%02d" % [hours, minutes, seconds]
	else:
		time_str = "%02d:%02d" % [minutes, seconds]

	return time_str

func _process(_delta: float) -> void:
	if state == TimerState.LOADING:
		return
	if state in [TimerState.STOPPED, TimerState.INVALID]:
		$'%StatusLabel'.text = 'Stopped.'
		return

	if timer_start_time == 0:
		return

	var current_time := OS.get_unix_time()
	var total_seconds = current_time - timer_start_time
	var time_str := seconds_to_timestamp(total_seconds)

	big_timer_label.text = time_str

	if state == TimerState.INTERRUPTED and interruption_start_time != 0:
		time_str += "\n* INTERRUPTED *\n"
		time_str += "[%s]" % seconds_to_timestamp(current_time - interruption_start_time)

	$'%StatusLabel'.text = time_str

func _on_DistractionFreeTimer_timeout() -> void:
	animation_player.play("StartDistractionFreeMode")

func _on_DistractionFreeMode_timer_touched() -> void:
	if animation_player.is_playing(): return
	animation_player.play("StopDistractionFreeMode")
	# TODO: Should work only if timer is active (Could display when Interupted if Current Overlay has a interuption support flag)
	# Temporary fix so timer doesn't display when interupted
	if state != TimerState.INTERRUPTED:
		distraction_free_timer.start()

func _unhandled_input(_event) -> void:
	if state == TimerState.STARTED:
		distraction_free_timer.start()