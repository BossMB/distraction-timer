extends CanvasLayer

const CREDENTIALS_FILENAME = 'user://credentials.json.enc'

var saved_username := ""
var saved_password := ""

func _ready() -> void:
	load_credentials()

func load_credentials() -> void:
	var file = File.new()
	file.open_encrypted_with_pass(CREDENTIALS_FILENAME, File.READ, Build.encryption_password)
	var result := JSON.parse(file.get_as_text())
	if result.result is Dictionary:
		saved_username = result.result['email']
		saved_password = result.result['password']
	file.close()

func save_credentials() -> void:
	var file = File.new()
	file.open_encrypted_with_pass(CREDENTIALS_FILENAME, File.WRITE, Build.encryption_password)
	var credentials = {
		email = saved_username,
		password = saved_password,
	}
	file.store_line(JSON.print(credentials))
	file.close()

func show_error(msg: String, title: String = "Error") -> void:
	var dialog = $'%AcceptDialog'
	dialog.window_title = title
	dialog.dialog_text = msg
	dialog.popup_centered()
