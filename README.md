Distraction Timer
=================

**A mobile app to help you stay focused!**

The idea is that you start the timer when doing an activity that you don't want
to distracted from. However, if you do get distracted, you hit the
"Interruption!" button and start tracking how long your distraction is. This can
then be used rate how well you stayed focused during that activity.

The goal is to give your focus a "score" to hopefully encourage you to keep that
score up!

This same approach has worked well for me in the past with calorie trackers:
even without setting specific calorie goals, just tracking it and seeing the
numbers has caused me to eat fewer calories.

Can the same work for distractions? Let's find out!

Local development
-----------------

This app is built with the [Godot game engine](https://godotengine.org/) v3.5
or later (but not Godot v4, only v3).

The data is stored in [Supabase](https://supabase.com/), which is an Open Source
alternative to Firebase.

To run the app locally:

1. Install [Docker](https://docs.docker.com/engine/install/).
2. Install the [Supabase CLI](https://github.com/supabase/cli).
3. Open a terminal to the directory where you downloaded the app's source code.
4. Run `supabase start` to run a local Supabase instance via Docker.  Once it's
   finished it'll output the "API URL" and "anon key" - save this for the next
   step!
5. Copy `addons/supabase/example.env` to `addons/supabase/.env` and update the
   "supabaseUrl" and "supabaseKey" to the values you saved from the previous
   step.
6. Run `supabase db reset` to import the database schema for this app into your
   local Supabase instance.
7. Open the project in the Godot editor, and run it!

License
-------

Copyright 2022 David Snopek.

Licensed under the [MIT License](LICENSE.txt).
